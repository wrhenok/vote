package com.ruoyi.vote.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 投票结果对象 vote_result
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public class VoteResult extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 结果id */
    private Long id;

    /** 投票人 */
    @Excel(name = "投票人")
    private String username;

    /** 投票id */
    @Excel(name = "投票id")
    private Long voteid;

    //  投票选项
    private List<Vote> votes;
    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setVoteid(Long voteid) 
    {
        this.voteid = voteid;
    }

    public Long getVoteid() 
    {
        return voteid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("username", getUsername())
            .append("voteid", getVoteid())
            .toString();
    }
}
