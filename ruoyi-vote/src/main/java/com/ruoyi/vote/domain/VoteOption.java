package com.ruoyi.vote.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 投票选项对象 vote_option
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public class VoteOption extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 投票选项 */
    private Long id;

    /** 投票id */
    @Excel(name = "投票id")
    private Long voteid;

    /** 选项内容 */
    @Excel(name = "选项内容")
    private String option;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setVoteid(Long voteid) 
    {
        this.voteid = voteid;
    }

    public Long getVoteid() 
    {
        return voteid;
    }
    public void setOption(String option) 
    {
        this.option = option;
    }

    public String getOption() 
    {
        return option;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("voteid", getVoteid())
            .append("option", getOption())
            .toString();
    }
}
