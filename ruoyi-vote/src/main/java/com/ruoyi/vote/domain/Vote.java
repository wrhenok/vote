package com.ruoyi.vote.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 投票对象 vote
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public class Vote extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 投票结果 */
    @Excel(name = "投票结果")
    private Long resultId;

    /** 投票选项 */
    @Excel(name = "投票选项")
    private Long optionId;

    private VoteOption option;

    public VoteOption getOption() {
        return option;
    }

    public void setOption(VoteOption option) {
        this.option = option;
    }



    public void setResultId(Long resultId) 
    {
        this.resultId = resultId;
    }

    public Long getResultId() 
    {
        return resultId;
    }
    public void setOptionId(Long optionId) 
    {
        this.optionId = optionId;
    }

    public Long getOptionId() 
    {
        return optionId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("resultId", getResultId())
            .append("optionId", getOptionId())
            .toString();
    }
}
