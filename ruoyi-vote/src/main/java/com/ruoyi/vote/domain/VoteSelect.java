package com.ruoyi.vote.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 投票管理对象 vote_select
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public class VoteSelect extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 投票ID */
    private Long id;

    /**
     * 用户名
     */
    @Excel(name = "用户名")
    private String username;

    /** 选项类型 */
    @Excel(name = "选项类型")
    private Long multi;

    /** 投票标题 */
    @Excel(name = "投票标题")
    private String title;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date begintime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date overtime;

    /** 是否匿名 */
    @Excel(name = "是否匿名")
    private Long anonymous;

    //    投票对应的选项
    private List<VoteOption> options;
    //    投票对应的结果
    private List<VoteResult> results;

    public List<VoteOption> getOptions() {
        return options;
    }

    public void setOptions(List<VoteOption> options) {
        this.options = options;
    }

    public List<VoteResult> getResults() {
        return results;
    }

    public void setResults(List<VoteResult> results) {
        this.results = results;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setMulti(Long multi)
    {
        this.multi = multi;
    }

    public Long getMulti() 
    {
        return multi;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setBegintime(Date begintime) 
    {
        this.begintime = begintime;
    }

    public Date getBegintime() 
    {
        return begintime;
    }
    public void setOvertime(Date overtime) 
    {
        this.overtime = overtime;
    }

    public Date getOvertime() 
    {
        return overtime;
    }
    public void setAnonymous(Long anonymous) 
    {
        this.anonymous = anonymous;
    }

    public Long getAnonymous() 
    {
        return anonymous;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("multi", getMulti())
            .append("title", getTitle())
            .append("description", getDescription())
            .append("begintime", getBegintime())
            .append("overtime", getOvertime())
            .append("anonymous", getAnonymous())
            .toString();
    }
}
