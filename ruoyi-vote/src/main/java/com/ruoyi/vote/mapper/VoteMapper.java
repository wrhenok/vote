package com.ruoyi.vote.mapper;

import java.util.List;
import com.ruoyi.vote.domain.Vote;

/**
 * 投票Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public interface VoteMapper 
{
    /**
     * 查询投票
     * 
     * @param resultId 投票主键
     * @return 投票
     */
    public Vote selectVoteByResultId(Long resultId);

    /**
     * 查询投票列表
     * 
     * @param vote 投票
     * @return 投票集合
     */
    public List<Vote> selectVoteList(Vote vote);

    /**
     * 新增投票
     * 
     * @param vote 投票
     * @return 结果
     */
    public int insertVote(Vote vote);

    /**
     * 修改投票
     * 
     * @param vote 投票
     * @return 结果
     */
    public int updateVote(Vote vote);

    /**
     * 删除投票
     * 
     * @param resultId 投票主键
     * @return 结果
     */
    public int deleteVoteByResultId(Long resultId);

    /**
     * 批量删除投票
     * 
     * @param resultIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteVoteByResultIds(String[] resultIds);
}
