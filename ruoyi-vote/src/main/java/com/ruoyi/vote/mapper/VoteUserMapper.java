package com.ruoyi.vote.mapper;

import java.util.List;
import com.ruoyi.vote.domain.VoteUser;

/**
 * voteUserMapper接口
 * 
 * @author ruoyi
 * @date 2022-10-11
 */
public interface VoteUserMapper 
{
    /**
     * 查询voteUser
     * 
     * @param id voteUser主键
     * @return voteUser
     */
    public VoteUser selectVoteUserById(Long id);

    /**
     * 查询voteUser列表
     * 
     * @param voteUser voteUser
     * @return voteUser集合
     */
    public List<VoteUser> selectVoteUserList(VoteUser voteUser);

    /**
     * 新增voteUser
     * 
     * @param voteUser voteUser
     * @return 结果
     */
    public int insertVoteUser(VoteUser voteUser);

    /**
     * 修改voteUser
     * 
     * @param voteUser voteUser
     * @return 结果
     */
    public int updateVoteUser(VoteUser voteUser);

    /**
     * 删除voteUser
     * 
     * @param id voteUser主键
     * @return 结果
     */
    public int deleteVoteUserById(Long id);

    /**
     * 批量删除voteUser
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteVoteUserByIds(String[] ids);
}
