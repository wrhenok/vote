package com.ruoyi.vote.mapper;

import java.util.List;
import com.ruoyi.vote.domain.VoteSelect;

/**
 * 投票管理Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public interface VoteSelectMapper 
{
    /**
     * 查询投票管理
     * 
     * @param id 投票管理主键
     * @return 投票管理
     */
    public VoteSelect selectVoteSelectById(Long id);

    /**
     * 查询投票管理列表
     * 
     * @param voteSelect 投票管理
     * @return 投票管理集合
     */
    public List<VoteSelect> selectVoteSelectList(VoteSelect voteSelect);

    /**
     * 新增投票管理
     * 
     * @param voteSelect 投票管理
     * @return 结果
     */
    public int insertVoteSelect(VoteSelect voteSelect);

    /**
     * 修改投票管理
     * 
     * @param voteSelect 投票管理
     * @return 结果
     */
    public int updateVoteSelect(VoteSelect voteSelect);

    /**
     * 删除投票管理
     * 
     * @param id 投票管理主键
     * @return 结果
     */
    public int deleteVoteSelectById(Long id);

    /**
     * 批量删除投票管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteVoteSelectByIds(String[] ids);
}
