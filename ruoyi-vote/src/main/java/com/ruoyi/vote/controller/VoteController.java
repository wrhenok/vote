package com.ruoyi.vote.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.vote.domain.Vote;
import com.ruoyi.vote.service.IVoteService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 投票Controller
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Controller
@RequestMapping("/vote/vote")
public class VoteController extends BaseController
{
    private String prefix = "vote/vote";

    @Autowired
    private IVoteService voteService;

    @RequiresPermissions("vote:vote:view")
    @GetMapping()
    public String vote()
    {
        return prefix + "/vote";
    }

    /**
     * 查询投票列表
     */
//    @RequiresPermissions("vote:vote:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Vote vote)
    {
        startPage();
        List<Vote> list = voteService.selectVoteList(vote);
        return getDataTable(list);
    }

    /**
     * 导出投票列表
     */
    @RequiresPermissions("vote:vote:export")
    @Log(title = "投票", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Vote vote)
    {
        List<Vote> list = voteService.selectVoteList(vote);
        ExcelUtil<Vote> util = new ExcelUtil<Vote>(Vote.class);
        return util.exportExcel(list, "投票数据");
    }

    /**
     * 新增投票
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存投票
     */
//    @RequiresPermissions("vote:vote:add")
    @Log(title = "投票", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Vote vote)
    {
        return toAjax(voteService.insertVote(vote));
    }

    @PostMapping("/add1")
    @ResponseBody
    public AjaxResult save(@RequestBody Vote vote) {
        int i = voteService.insertVote(vote);
        if (i > 0) {
            return AjaxResult.success("添加成功");
        }
        return AjaxResult.error("添加失败");
    }

    /**
     * 修改投票
     */
//    @RequiresPermissions("vote:vote:edit")
    @GetMapping("/edit/{resultId}")
    public String edit(@PathVariable("resultId") Long resultId, ModelMap mmap)
    {
        Vote vote = voteService.selectVoteByResultId(resultId);
        mmap.put("vote", vote);
        return prefix + "/edit";
    }

    /**
     * 修改保存投票
     */
//    @RequiresPermissions("vote:vote:edit")
    @Log(title = "投票", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Vote vote)
    {
        return toAjax(voteService.updateVote(vote));
    }

    /**
     * 删除投票
     */
//    @RequiresPermissions("vote:vote:remove")
    @Log(title = "投票", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(voteService.deleteVoteByResultIds(ids));
    }
}
