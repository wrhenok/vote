package com.ruoyi.vote.controller;

import java.util.List;

import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.vote.domain.VoteSelect;
import com.ruoyi.vote.service.IVoteSelectService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 投票管理Controller
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Controller
@RequestMapping("/vote/select")
public class VoteSelectController extends BaseController
{
    private String prefix = "vote/select";

    @Autowired
    private IVoteSelectService voteSelectService;

    @GetMapping()
    public String select()
    {
        return prefix + "/select";
    }

    /**
     * 查询投票管理列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(VoteSelect voteSelect)
    {
        startPage();
        List<VoteSelect> list = voteSelectService.selectVoteSelectList(voteSelect);
        return getDataTable(list);
    }

    @GetMapping("/lists")
    @ResponseBody
    public List<VoteSelect> lists() {
        return voteSelectService.selectVoteSelectList(null);
    }

    @GetMapping("/lists/{id}")
    @ResponseBody
    public VoteSelect listById(@PathVariable Long id) {
        return voteSelectService.selectVoteSelectById(id);
    }

    @ApiOperation("通过用户名查询投票")
    @GetMapping("/list")
    @ResponseBody
    public AjaxResult listByUsername(String username) {
        VoteSelect vote = new VoteSelect();
        vote.setUsername(username);
        List<VoteSelect> votes = voteSelectService.selectVoteSelectList(vote);
        if (votes != null) {
            return AjaxResult.success("查询成功", votes);
        }
        return AjaxResult.error("查询失败");
    }

    /**
     * 导出投票管理列表
     */
    @RequiresPermissions("vote:select:export")
    @Log(title = "投票管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(VoteSelect voteSelect)
    {
        List<VoteSelect> list = voteSelectService.selectVoteSelectList(voteSelect);
        ExcelUtil<VoteSelect> util = new ExcelUtil<VoteSelect>(VoteSelect.class);
        return util.exportExcel(list, "投票管理数据");
    }

    /**
     * 新增投票管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存投票管理
     */
    @RequiresPermissions("vote:select:add")
    @Log(title = "投票管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(VoteSelect voteSelect)
    {
        return toAjax(voteSelectService.insertVoteSelect(voteSelect));
    }

    /**
     * 新增保存投票信息
     */
    @ApiOperation("新增保存投票信息")
    @Log(title = "投票信息", businessType = BusinessType.INSERT)
    @PostMapping("/add1")
    @ResponseBody
    public AjaxResult add(@RequestBody VoteSelect vote) {
        int i = voteSelectService.insertVoteSelect(vote);
        if (i > 0) {
            return AjaxResult.success("添加成功", vote.getId());
        }
        return AjaxResult.error("添加失败");
    }

    /**
     * 修改投票管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        VoteSelect voteSelect = voteSelectService.selectVoteSelectById(id);
        mmap.put("voteSelect", voteSelect);
        return prefix + "/edit";
    }

    /**
     * 修改保存投票管理
     */
    @ApiOperation("修改保存投票")
    @Log(title = "投票管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(VoteSelect voteSelect)
    {
        return toAjax(voteSelectService.updateVoteSelect(voteSelect));
    }

    @ApiOperation("修改保存投票信息")
    @Log(title = "投票信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit1")
    @ResponseBody
    public AjaxResult edit1(@RequestBody VoteSelect vote) {
        return toAjax(voteSelectService.updateVoteSelect(vote));
    }

    /**
     * 删除投票管理
     */
    @ApiOperation("删除投票信息")
    @Log(title = "投票管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(voteSelectService.deleteVoteSelectByIds(ids));
    }
}
