package com.ruoyi.vote.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.vote.domain.VoteOption;
import com.ruoyi.vote.service.IVoteOptionService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 投票选项Controller
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Controller
@RequestMapping("/vote/option")
public class VoteOptionController extends BaseController
{
    private String prefix = "vote/option";

    @Autowired
    private IVoteOptionService voteOptionService;

    @RequiresPermissions("vote:option:view")
    @GetMapping()
    public String option()
    {
        return prefix + "/option";
    }

    /**
     * 查询投票选项列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(VoteOption voteOption)
    {
        startPage();
        List<VoteOption> list = voteOptionService.selectVoteOptionList(voteOption);
        return getDataTable(list);
    }

    @GetMapping("/list/{voteId}")
    @ResponseBody
    public List<VoteOption> find(@PathVariable Long voteId) {
        return voteOptionService.selectVoteOptionByVoteId(voteId);
    }

    @GetMapping("/list")
    @ResponseBody
    public AjaxResult listByVoteId(Long voteId) {
        VoteOption options = new VoteOption();
        options.setVoteid(voteId);
        List<VoteOption> list = voteOptionService.selectVoteOptionList(options);
        return list != null ? AjaxResult.success("查询成功", list) : AjaxResult.error("查询失败");
    }


    /**
     * 导出投票选项列表
     */
    @RequiresPermissions("vote:option:export")
    @Log(title = "投票选项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(VoteOption voteOption)
    {
        List<VoteOption> list = voteOptionService.selectVoteOptionList(voteOption);
        ExcelUtil<VoteOption> util = new ExcelUtil<VoteOption>(VoteOption.class);
        return util.exportExcel(list, "投票选项数据");
    }

    /**
     * 新增投票选项
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存投票选项
     */
    @Log(title = "投票选项", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(VoteOption voteOption)
    {
        return toAjax(voteOptionService.insertVoteOption(voteOption));
    }

    @PostMapping("/add1/{voteId}")
    @ResponseBody
    public AjaxResult add1(@PathVariable Long voteId, @RequestBody String[] ops) {
        boolean flag = true;
        for (String op : ops) {
            VoteOption options = new VoteOption();
            options.setVoteid(voteId);
            options.setOption(op);
            int i = voteOptionService.insertVoteOption(options);
            if (i <= 0) {
                flag = false;
            }
        }
        return flag ? AjaxResult.success("添加成功") : AjaxResult.error("添加失败");
    }

    /**
     * 修改投票选项
     */
    @RequiresPermissions("vote:option:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        VoteOption voteOption = voteOptionService.selectVoteOptionById(id);
        mmap.put("voteOption", voteOption);
        return prefix + "/edit";
    }

    /**
     * 修改保存投票选项
     */
    @Log(title = "投票选项", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(VoteOption voteOption)
    {
        return toAjax(voteOptionService.updateVoteOption(voteOption));
    }

    @PostMapping("/edit1")
    @ResponseBody
    public AjaxResult edit1(@RequestBody VoteOption[] options) {
        boolean flag = true;
        for (VoteOption option : options) {
            if (option.getId() == null) {
                int i = voteOptionService.insertVoteOption(option);
                if (i <= 0) {
                    flag = false;
                }
            } else {
                int i = voteOptionService.updateVoteOption(option);
                if (i <= 0) {
                    flag = false;
                }
            }

        }
        return flag ? AjaxResult.success() : AjaxResult.error();
    }

    /**
     * 删除投票选项
     */
    @Log(title = "投票选项", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(voteOptionService.deleteVoteOptionByIds(ids));
    }
}
