package com.ruoyi.vote.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.vote.domain.VoteResult;
import com.ruoyi.vote.service.IVoteResultService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 投票结果Controller
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Controller
@RequestMapping("/vote/result")
public class VoteResultController extends BaseController
{
    private String prefix = "vote/result";

    @Autowired
    private IVoteResultService voteResultService;

    @RequiresPermissions("vote:result:view")
    @GetMapping()
    public String result()
    {
        return prefix + "/result";
    }

    /**
     * 查询投票结果列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(VoteResult voteResult)
    {
        startPage();
        List<VoteResult> list = voteResultService.selectVoteResultList(voteResult);
        return getDataTable(list);
    }

    /**
     * 导出投票结果列表
     */
    @RequiresPermissions("vote:result:export")
    @Log(title = "投票结果", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(VoteResult voteResult)
    {
        List<VoteResult> list = voteResultService.selectVoteResultList(voteResult);
        ExcelUtil<VoteResult> util = new ExcelUtil<VoteResult>(VoteResult.class);
        return util.exportExcel(list, "投票结果数据");
    }

    /**
     * 新增投票结果
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存投票结果
     */
    @Log(title = "投票结果", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(VoteResult voteResult)
    {
        return toAjax(voteResultService.insertVoteResult(voteResult));
    }

    @Log(title = "结果信息", businessType = BusinessType.INSERT)
    @PostMapping("/add1")
    @ResponseBody
    public AjaxResult save(@RequestBody VoteResult result) {
        int i = voteResultService.insertVoteResult(result);
        if (i > 0) {
            return AjaxResult.success("添加成功", result.getId());
        }
        return AjaxResult.error("添加失败");
    }

    /**
     * 修改投票结果
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        VoteResult voteResult = voteResultService.selectVoteResultById(id);
        mmap.put("voteResult", voteResult);
        return prefix + "/edit";
    }

    /**
     * 修改保存投票结果
     */
    @Log(title = "投票结果", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(VoteResult voteResult)
    {
        return toAjax(voteResultService.updateVoteResult(voteResult));
    }

    /**
     * 删除投票结果
     */
    @RequiresPermissions("vote:result:remove")
    @Log(title = "投票结果", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(voteResultService.deleteVoteResultByIds(ids));
    }
}
