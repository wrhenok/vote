package com.ruoyi.vote.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.vote.domain.VoteUser;
import com.ruoyi.vote.service.IVoteUserService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * voteUserController
 * 
 * @author ruoyi
 * @date 2022-10-11
 */
@Controller
@RequestMapping("/vote/user")
public class VoteUserController extends BaseController
{
    private String prefix = "vote/user";

    @Autowired
    private IVoteUserService voteUserService;

    @RequiresPermissions("vote:user:view")
    @GetMapping()
    public String user()
    {
        return prefix + "/user";
    }

    /**
     * 查询voteUser列表
     */
    @RequiresPermissions("vote:user:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(VoteUser voteUser)
    {
        startPage();
        List<VoteUser> list = voteUserService.selectVoteUserList(voteUser);
        return getDataTable(list);
    }

    /**
     * 导出voteUser列表
     */
    @RequiresPermissions("vote:user:export")
    @Log(title = "voteUser", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(VoteUser voteUser)
    {
        List<VoteUser> list = voteUserService.selectVoteUserList(voteUser);
        ExcelUtil<VoteUser> util = new ExcelUtil<VoteUser>(VoteUser.class);
        return util.exportExcel(list, "voteUser数据");
    }

    /**
     * 新增voteUser
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存voteUser
     */
    @RequiresPermissions("vote:user:add")
    @Log(title = "voteUser", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(VoteUser voteUser)
    {
        return toAjax(voteUserService.insertVoteUser(voteUser));
    }

    /**
     * 修改voteUser
     */
    @RequiresPermissions("vote:user:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        VoteUser voteUser = voteUserService.selectVoteUserById(id);
        mmap.put("voteUser", voteUser);
        return prefix + "/edit";
    }

    /**
     * 修改保存voteUser
     */
    @RequiresPermissions("vote:user:edit")
    @Log(title = "voteUser", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(VoteUser voteUser)
    {
        return toAjax(voteUserService.updateVoteUser(voteUser));
    }

    /**
     * 删除voteUser
     */
    @RequiresPermissions("vote:user:remove")
    @Log(title = "voteUser", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(voteUserService.deleteVoteUserByIds(ids));
    }
}
