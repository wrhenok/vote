package com.ruoyi.vote.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.vote.mapper.VoteResultMapper;
import com.ruoyi.vote.domain.VoteResult;
import com.ruoyi.vote.service.IVoteResultService;
import com.ruoyi.common.core.text.Convert;

/**
 * 投票结果Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Service
public class VoteResultServiceImpl implements IVoteResultService 
{
    @Autowired
    private VoteResultMapper voteResultMapper;

    /**
     * 查询投票结果
     * 
     * @param id 投票结果主键
     * @return 投票结果
     */
    @Override
    public VoteResult selectVoteResultById(Long id)
    {
        return voteResultMapper.selectVoteResultById(id);
    }

    /**
     * 查询投票结果列表
     * 
     * @param voteResult 投票结果
     * @return 投票结果
     */
    @Override
    public List<VoteResult> selectVoteResultList(VoteResult voteResult)
    {
        return voteResultMapper.selectVoteResultList(voteResult);
    }

    /**
     * 新增投票结果
     * 
     * @param voteResult 投票结果
     * @return 结果
     */
    @Override
    public int insertVoteResult(VoteResult voteResult)
    {
        return voteResultMapper.insertVoteResult(voteResult);
    }

    /**
     * 修改投票结果
     * 
     * @param voteResult 投票结果
     * @return 结果
     */
    @Override
    public int updateVoteResult(VoteResult voteResult)
    {
        return voteResultMapper.updateVoteResult(voteResult);
    }

    /**
     * 批量删除投票结果
     * 
     * @param ids 需要删除的投票结果主键
     * @return 结果
     */
    @Override
    public int deleteVoteResultByIds(String ids)
    {
        return voteResultMapper.deleteVoteResultByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除投票结果信息
     * 
     * @param id 投票结果主键
     * @return 结果
     */
    @Override
    public int deleteVoteResultById(Long id)
    {
        return voteResultMapper.deleteVoteResultById(id);
    }
}
