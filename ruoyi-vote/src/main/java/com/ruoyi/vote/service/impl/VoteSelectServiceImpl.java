package com.ruoyi.vote.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.vote.mapper.VoteSelectMapper;
import com.ruoyi.vote.domain.VoteSelect;
import com.ruoyi.vote.service.IVoteSelectService;
import com.ruoyi.common.core.text.Convert;

/**
 * 投票管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Service
public class VoteSelectServiceImpl implements IVoteSelectService 
{
    @Autowired
    private VoteSelectMapper voteSelectMapper;

    /**
     * 查询投票管理
     * 
     * @param id 投票管理主键
     * @return 投票管理
     */
    @Override
    public VoteSelect selectVoteSelectById(Long id)
    {
        return voteSelectMapper.selectVoteSelectById(id);
    }

    /**
     * 查询投票管理列表
     * 
     * @param voteSelect 投票管理
     * @return 投票管理
     */
    @Override
    public List<VoteSelect> selectVoteSelectList(VoteSelect voteSelect)
    {
        return voteSelectMapper.selectVoteSelectList(voteSelect);
    }

    /**
     * 新增投票管理
     * 
     * @param voteSelect 投票管理
     * @return 结果
     */
    @Override
    public int insertVoteSelect(VoteSelect voteSelect)
    {
        return voteSelectMapper.insertVoteSelect(voteSelect);
    }

    /**
     * 修改投票管理
     * 
     * @param voteSelect 投票管理
     * @return 结果
     */
    @Override
    public int updateVoteSelect(VoteSelect voteSelect)
    {
        return voteSelectMapper.updateVoteSelect(voteSelect);
    }

    /**
     * 批量删除投票管理
     * 
     * @param ids 需要删除的投票管理主键
     * @return 结果
     */
    @Override
    public int deleteVoteSelectByIds(String ids)
    {
        return voteSelectMapper.deleteVoteSelectByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除投票管理信息
     * 
     * @param id 投票管理主键
     * @return 结果
     */
    @Override
    public int deleteVoteSelectById(Long id)
    {
        return voteSelectMapper.deleteVoteSelectById(id);
    }
}
