package com.ruoyi.vote.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.vote.mapper.VoteMapper;
import com.ruoyi.vote.domain.Vote;
import com.ruoyi.vote.service.IVoteService;
import com.ruoyi.common.core.text.Convert;

/**
 * 投票Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Service
public class VoteServiceImpl implements IVoteService 
{
    @Autowired
    private VoteMapper voteMapper;

    /**
     * 查询投票
     * 
     * @param resultId 投票主键
     * @return 投票
     */
    @Override
    public Vote selectVoteByResultId(Long resultId)
    {
        return voteMapper.selectVoteByResultId(resultId);
    }

    /**
     * 查询投票列表
     * 
     * @param vote 投票
     * @return 投票
     */
    @Override
    public List<Vote> selectVoteList(Vote vote)
    {
        return voteMapper.selectVoteList(vote);
    }

    /**
     * 新增投票
     * 
     * @param vote 投票
     * @return 结果
     */
    @Override
    public int insertVote(Vote vote)
    {
        return voteMapper.insertVote(vote);
    }

    /**
     * 修改投票
     * 
     * @param vote 投票
     * @return 结果
     */
    @Override
    public int updateVote(Vote vote)
    {
        return voteMapper.updateVote(vote);
    }

    /**
     * 批量删除投票
     * 
     * @param resultIds 需要删除的投票主键
     * @return 结果
     */
    @Override
    public int deleteVoteByResultIds(String resultIds)
    {
        return voteMapper.deleteVoteByResultIds(Convert.toStrArray(resultIds));
    }

    /**
     * 删除投票信息
     * 
     * @param resultId 投票主键
     * @return 结果
     */
    @Override
    public int deleteVoteByResultId(Long resultId)
    {
        return voteMapper.deleteVoteByResultId(resultId);
    }
}
