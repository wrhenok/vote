package com.ruoyi.vote.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.vote.mapper.VoteOptionMapper;
import com.ruoyi.vote.domain.VoteOption;
import com.ruoyi.vote.service.IVoteOptionService;
import com.ruoyi.common.core.text.Convert;

/**
 * 投票选项Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Service
public class VoteOptionServiceImpl implements IVoteOptionService 
{
    @Autowired
    private VoteOptionMapper voteOptionMapper;

    /**
     * 查询投票选项
     * 
     * @param id 投票选项主键
     * @return 投票选项
     */
    @Override
    public VoteOption selectVoteOptionById(Long id)
    {
        return voteOptionMapper.selectVoteOptionById(id);
    }



    /**
     * 查询投票选项列表
     * 
     * @param voteOption 投票选项
     * @return 投票选项
     */
    @Override
    public List<VoteOption> selectVoteOptionList(VoteOption voteOption) {
        return voteOptionMapper.selectVoteOptionList(voteOption);
    }

    @Override
    public List<VoteOption> selectVoteOptionByVoteId(Long voteId) {
        return voteOptionMapper.selectVoteOptionByVoteId(voteId);
    }

    /**
     * 新增投票选项
     * 
     * @param voteOption 投票选项
     * @return 结果
     */
    @Override
    public int insertVoteOption(VoteOption voteOption)
    {
        return voteOptionMapper.insertVoteOption(voteOption);
    }

    /**
     * 修改投票选项
     * 
     * @param voteOption 投票选项
     * @return 结果
     */
    @Override
    public int updateVoteOption(VoteOption voteOption)
    {
        return voteOptionMapper.updateVoteOption(voteOption);
    }

    /**
     * 批量删除投票选项
     * 
     * @param ids 需要删除的投票选项主键
     * @return 结果
     */
    @Override
    public int deleteVoteOptionByIds(String ids)
    {
        return voteOptionMapper.deleteVoteOptionByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除投票选项信息
     * 
     * @param id 投票选项主键
     * @return 结果
     */
    @Override
    public int deleteVoteOptionById(Long id)
    {
        return voteOptionMapper.deleteVoteOptionById(id);
    }
}
