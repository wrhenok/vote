package com.ruoyi.vote.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.vote.mapper.VoteUserMapper;
import com.ruoyi.vote.domain.VoteUser;
import com.ruoyi.vote.service.IVoteUserService;
import com.ruoyi.common.core.text.Convert;

/**
 * voteUserService业务层处理
 * 
 * @author ruoyi
 * @date 2022-10-11
 */
@Service
public class VoteUserServiceImpl implements IVoteUserService 
{
    @Autowired
    private VoteUserMapper voteUserMapper;

    /**
     * 查询voteUser
     * 
     * @param id voteUser主键
     * @return voteUser
     */
    @Override
    public VoteUser selectVoteUserById(Long id)
    {
        return voteUserMapper.selectVoteUserById(id);
    }

    /**
     * 查询voteUser列表
     * 
     * @param voteUser voteUser
     * @return voteUser
     */
    @Override
    public List<VoteUser> selectVoteUserList(VoteUser voteUser)
    {
        return voteUserMapper.selectVoteUserList(voteUser);
    }

    /**
     * 新增voteUser
     * 
     * @param voteUser voteUser
     * @return 结果
     */
    @Override
    public int insertVoteUser(VoteUser voteUser)
    {
        return voteUserMapper.insertVoteUser(voteUser);
    }

    /**
     * 修改voteUser
     * 
     * @param voteUser voteUser
     * @return 结果
     */
    @Override
    public int updateVoteUser(VoteUser voteUser)
    {
        return voteUserMapper.updateVoteUser(voteUser);
    }

    /**
     * 批量删除voteUser
     * 
     * @param ids 需要删除的voteUser主键
     * @return 结果
     */
    @Override
    public int deleteVoteUserByIds(String ids)
    {
        return voteUserMapper.deleteVoteUserByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除voteUser信息
     * 
     * @param id voteUser主键
     * @return 结果
     */
    @Override
    public int deleteVoteUserById(Long id)
    {
        return voteUserMapper.deleteVoteUserById(id);
    }
}
