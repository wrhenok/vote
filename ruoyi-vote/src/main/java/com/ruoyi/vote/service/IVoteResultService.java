package com.ruoyi.vote.service;

import java.util.List;
import com.ruoyi.vote.domain.VoteResult;

/**
 * 投票结果Service接口
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
public interface IVoteResultService 
{
    /**
     * 查询投票结果
     * 
     * @param id 投票结果主键
     * @return 投票结果
     */
    public VoteResult selectVoteResultById(Long id);

    /**
     * 查询投票结果列表
     * 
     * @param voteResult 投票结果
     * @return 投票结果集合
     */
    public List<VoteResult> selectVoteResultList(VoteResult voteResult);

    /**
     * 新增投票结果
     * 
     * @param voteResult 投票结果
     * @return 结果
     */
    public int insertVoteResult(VoteResult voteResult);

    /**
     * 修改投票结果
     * 
     * @param voteResult 投票结果
     * @return 结果
     */
    public int updateVoteResult(VoteResult voteResult);

    /**
     * 批量删除投票结果
     * 
     * @param ids 需要删除的投票结果主键集合
     * @return 结果
     */
    public int deleteVoteResultByIds(String ids);

    /**
     * 删除投票结果信息
     * 
     * @param id 投票结果主键
     * @return 结果
     */
    public int deleteVoteResultById(Long id);
}
